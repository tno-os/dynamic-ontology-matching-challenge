# Dynamic Ontology Matching Challenge

## Description
This repository contains the data that is to be used in the Dynamic Ontology Matching Challenge as presented in the AAAI-MAKE symposium. More information about the timeline and submissions will follow.

The goal of the challenge is to create the (occupation) alignment of the ESCO 2017 version to O*NET (2019) (1) and the new ESCO (2022) version (2). It is necessary to use a hybrid method in this challenge, and it is allowed to use some human annotation in the challenge. Directions of solutions can be optimizing the combination of data-driven ontology mapping techniques and the (currently manually captured) expert-based knowledge (models), but also solutions that address the current challenges in ontology mapping.

The winner of the challenge will be selected using the following criteria:
1) novelty of the method used
2) performance: the number of correct matches
3) scalability: can it work real-time or near real-time


Participants are invited to broaden the challenge to for example mapping to other ontologies, mapping of skills as well as occupations, use different type of relations (such as broader than / narrower than) and/or map to other languages.

## Data 
The data for this challenge has been gathered for you in the `data/` zip file. 
In this folder you will find the occupations as listed in ESCO and O\*NET in different versions. - `ESCO_2017_occupations_en.csv` containing ESCO occupations version 1.0.9
- `ESCO_2022_occupations_en.csv` containing ESCO occupations version 1.1.1
- `ONET_2019_Occupations.csv` containing O\*NET occupations as updated in 2019
- `Mapping_ESCO2022_ONET2019.csv` containing a mapping of O\*NET 2019 to ESCO version 1.1.1 Additionally, there are metadata files corresponding to each of the data files. The original ESCO occupation files and further information can be found [here.](https://esco.ec.europa.eu/en/use-esco/download)
Information on the O\*NET occupations can be found [here.](https://www.onetcenter.org/taxonomy/2019/list.html)
The link to the O\*NET-ESCO crosswalk can be found [here.](https://esco.ec.europa.eu/en/use-esco/other-crosswalks) to find the original O\*NET-ESCO crosswalk.


## Support
Please contact Maaike de Boer (maaike.deboer@tno.nl) or Quirine Smit (quirine.smit@tno.nl) for questions.

## Authors and acknowledgment
This service uses the ESCO classification of the European Commission.

## License
TBD
